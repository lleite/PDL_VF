Projet PDL 2021 réalisé par Lucien Leite

lien ssh : git@gitlab.emi.u-bordeaux.fr:lleite/PDL_VF.git
dernier commit : 0436f3697d5acd7d42602aa10a23dcc87f7f59fa

1 Serveur :

    Travail réalisé :

    lorsque le serveur est lancé il enregistre toutes les images du dossier images.
    le serveur peut : accéder à une image via son identifiant, 
    supprimer une image via son identifiant, ajouter une image, construire la liste des images disponibles, 
    
2 Communication + Client:

    Travail réalisé :

    la liste des images du dossier apparait sur la page au lancement du serveur,
    cliquer sur l'une de ces images affiche l'image selectionnée au milieu de la page,
    ajout d'un bouton "supprimer" qui supprime l'image qui a été précedement séléctionnée.
    début de l'ajout d'une fenetre de dépot d'image a poster sur le serveur.
